# RapidHaskell Workshop 3

To build

```{.bash}
stack build
stack exec -- app-exe
```

Slides are [here](http://rapidhaskell2018.gitlab.io/workshops/3.pdf)
