module Main where

import RIO
import Data.Yaml
import Data.Yaml.Pretty

data V3 a = V3 a a a
  deriving (Eq, Show, Functor)

data Job = Milkman | Postman
  deriving (Eq, Show, Generic)

instance FromJSON Job
instance ToJSON Job

data Dragon = Dragon {
  name  :: String
, heads :: Int
, job   :: Maybe Job
} deriving (Eq, Show, Generic)

instance FromJSON Dragon
instance ToJSON Dragon

data Material = Oak | CarbonFibre | Smiles | Gold
  deriving (Eq, Show, Generic)

instance FromJSON Material
instance ToJSON Material

data Wagon a = Wagon {
  material :: Material
, contents :: a
} deriving (Eq, Show, Generic, Functor)

instance FromJSON a => FromJSON (Wagon a)
instance ToJSON a => ToJSON (Wagon a)

instance Semigroup Dragon where
  Dragon n1 h1 j <> Dragon n2 h2 _ =
    Dragon (n1 <> n2) (h1 + h2) j

instance Monoid Dragon where
  mempty = Dragon "" 0 Nothing

main = do
  (d :: Wagon [Dragon]) <- decodeFileThrow "data/config.yaml"
  logStdout <- logOptionsHandle stdout True
  withLogFunc logStdout $ \lfs ->
    runRIO lfs $ do
      logInfo $ displayShow d
      logInfo $ displayBytesUtf8 $ encodePretty defConfig d
      logDebug "Goodbye"
      logWarn "What the..."
      logError "OHNO!"
